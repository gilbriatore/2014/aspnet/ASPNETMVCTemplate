﻿using ASPNETMVCTemplate.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASPNETMVCTemplate.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Usuario u = new Usuario();
            u.Nome = "João";
            u.IsAtivo = true;
            u.Observacoes = "Um bloco de texto";
            u.Preferencias = new string[] { "Branco", "Verde" };
            u.Sexo = "Masculino";

            return View(u);
        }

        public ActionResult ExemploHelpers()
        {
            Grupo g1 = new Grupo();
            g1.Id = 1;
            g1.Descricao = "Alimentos";

            Grupo g2 = new Grupo();
            g2.Id = 1;
            g1.Descricao = "Bebidas";

            List<Grupo> grupos = new List<Grupo>();
            grupos.Add(g1);
            grupos.Add(g2);

            //ViewData
            ViewBag.Grupos = grupos;

            return View(new Categoria());
        }

        [HttpPost]
        public ActionResult ExemploHelperPost(Categoria categoria)
        {
            //1. Request: Request["Descricao"];
            //2. FormCollection: form["Descricao"];
            //3. Categoria: categoria
            

            return View();
        }


        public ActionResult Page()
        {
            return View();
        }

        public ActionResult Examples()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        public ActionResult AnotherPage()
        {
            return View();
        }

    }
}
