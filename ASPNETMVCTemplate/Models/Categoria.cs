﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ASPNETMVCTemplate.Models
{
    public class Categoria
    {
        public int Id { get; set; }

        [Display(Name="Descrição")]
        [Required(ErrorMessage = "O nome é obrigatório!")]
        public string Descricao { get; set; }
        public Grupo Grupo { get; set; }
    }
}