﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ASPNETMVCTemplate.Models
{
    public class Usuario
    {
        public int Id { get; set; }

        [DisplayName("Primeiro Nome")]
        public string Nome { get; set; }

        [DisplayName("Segundo Nome")]
        public string Sobrenome { get; set; }

        public DateTime DataDeCadastro { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DataDeNascimento { get; set; }

        public string Senha { get; set; }

        public bool IsAtivo { get; set; }

        public string Sexo { get; set; }

        public string[] Preferencias { get; set; }

        public string Observacoes { get; set; }
    }
}